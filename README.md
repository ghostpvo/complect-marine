УСТАНОВКА GULP 
=====================
$ npm i  Установить зависимости

$ gulp server  Запустить сборку, сервер и слежение за файлами

http://localhost:80



ОСНОВНЫЕ ПРАВИЛА ПРОЕКТА 
=====================
* Используется именование классов, файлов и переменных по БЭМ.
https://ru.bem.info/methodology/quick-start/

* Каждый БЭМ-блок в своей папке внутри ./src/blocks/ (стили, js, картинки, разметка; обязателен только стилевой файл).

* Есть глобальные файлы: стилевые, js, шрифты, картинки.

* Диспетчер подключения стилей ./src/less/style.less генерируется автоматически при старте любой gulp-задачи.


ОСНОВНЫЕ КРИТЕРИИ ВЁРСТКИ
=====================



БЛОКИ
=====================
Каждый блок лежит в ./src/blocks/ в своей папке. Каждый блок — как минимум, папка и одноимённый less-файл.

Возможное содержимое блока:

demo-block/               # Папка блока
  img/                    # Изображения, используемые блоком и обрабатываемые автоматикой сборки
  some-folder/            # Какая-то сторонняя папка, не обрабатываемая автоматикой
  demo-block.less         # Стилевой файл блока
  demo-block--mod.less    # Отдельный стилевой файл БЭМ-модификатора блока
  demo-block.js           # js-файл блока
  demo-block--mod.js      # js-файл для отдельного БЭМ-модификатора блока
  demo-block.html         # Варианты разметки (как документация блока или как вставляемый микрошаблонизатором фрагмент)
  readme.md               # Какое-то пояснение


СТРУКТУРА ПАПОК И ФАЙЛОВ
=====================
src
├───assets
│   └───bemtopug
│
├───blocks
│   └───blockName
│       ├───blockName.pug
│       └───blockName.less
│
├───fonts
│   ├───font.ttf
│   ├───font.woff
│   └───font.woff2
│
├───pug
│   ├───connect
│   │   └───_blocks.pug
│   │
│   ├───layout
│   │   └───_layoutBase.pug
│   │
│   ├───pages
│   │   └───index.pug
│   │
│   └───template
│       ├───_head.pug
│       ├───_header.pug
│       └───_footer.pug
│
├───less
│   ├───common
│   │   ├───_grid.less
│   │   ├───_fonts.less
│   │   ├───_png-sprite.less
│   │   └───_variables.less
│   │
│   ├───vendor
│   │   ├───normalize.css
│   │   └───bootstrap
│   │
│   ├───mixin
│   │   └───_clearfix.less
│   │
│   └───style.less (диспетчер подключений .less файлов)
│
├───js
│   ├───vendor
│   │   ├───jQuery.js
│   │   └───etc.js
│   │
│   └───scripts
│       └───myscript.js
│
└───img
    ├───png-sprite
    │   ├───file1.png
    │   └───file2.png
    │
    ├───images1.jpg
    └───images2.png
